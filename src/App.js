import React, { Component } from 'react';
import './App.css';
import GridRow from "./components/GridRow";
import Cell from './components/Cell';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            key: 'ZR7AWSTB4T630DJO',
            ticker: 'KO',
            lastLoad: '',
            price: 0,
            dividend: 0,
            payoutMonth: 0,
            start: 1000,
            monthly: 100,
            emulation: [],
            year: new Date().getFullYear(),
            month: Math.floor(new Date().getMonth()) + 1
        };

        //this.refreshStockData();
    }

    refreshStockData() {
        fetch(`https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol=${this.state.ticker}&apikey=${this.state.key}`)
            .then(res => res.json())
            .then(result => {
                if(this.state.lastLoad === this.state.ticker)
                    return;

                let mats = Object.entries(result["Monthly Adjusted Time Series"]);
                let d = 0;
                let pm = 0;
                for(let i = 0; i < mats.length; i++){
                    if(parseFloat(mats[i][1]["7. dividend amount"]) !== 0) {
                        d = mats[i][1]["7. dividend amount"];
                        pm = (new Date(mats[i][0]).getMonth())%4;
                        break;
                    }
                }
                let price = (parseFloat(mats[0][1]["2. high"]) + parseFloat(mats[0][1]["3. low"]))/2;

                this.setState({
                    dividend: d,
                    price: price,
                    payoutMonth: pm,
                    lastLoad: this.state.ticker
                });

            }).then(() => this.emulateYears());
    }

    //emulate it by month
    emulateYears(){
        let startMoney = this.state.start;
        let monthly = this.state.monthly;
        let dividend = this.state.dividend;
        let price = this.state.price;
        let shares = Math.floor(startMoney/price);
        //let month = this.state.month;
        let year = this.state.year;
        let dividendPay = 0;
        let leftover = startMoney - shares*this.state.price;
        let total = startMoney;
        let invested = startMoney;

        let all = [];

        while(year < 2040) {
            let startMonth = year === this.state.year? this.state.month:1;

            for (let m = startMonth; m <= 12; m++) {
                all.push({
                    year: year,
                    month: m,
                    dividendPay: dividendPay,
                    shares: shares,
                    leftover: leftover,
                    invested: invested,
                    total: total
                });
                invested += monthly;
                leftover += monthly;
                if(m % 4 === this.state.payoutMonth) {
                    dividendPay = shares * dividend;
                    leftover += dividendPay;
                }
                else
                    dividendPay = 0;

                if(leftover >= price){
                    let buy = Math.floor(leftover/price);
                    leftover -= buy*price;
                    shares += buy;
                }
                total = leftover + shares*price;
            }
            year++;
        }

        this.setState({
            emulation: all
        });
    }

    changeTicker(e){
        this.setState({ticker: e.target.value});
    }

    changeMonthly(e){
        this.setState({monthly: parseFloat(e.target.value)});
    }

    changeStart(e){
        this.setState({start: parseFloat(e.target.value)});
    }

    render() {
        const gridRows = this.state.emulation.map(step => {
            return <GridRow step={step} key={step.month+step.year*12}/>;
        });

        const top = <div style={{backgroundColor: 'black', color: 'white', display: 'flex', width: 'fit-content'}}>
            <Cell header={true}>Time</Cell>
            <Cell header={true}>Payout</Cell>
            <Cell header={true}>Shares</Cell>
            <Cell header={true}>Leftover</Cell>
            <Cell header={true}>Invested</Cell>
            <Cell header={true}>Total</Cell>
        </div>;

        return (
            <div style={{contentAlign: 'center', align: 'center'}}>
                <textarea onChange={e => this.changeTicker(e)} value={this.state.ticker}/>
                <textarea onChange={e => this.changeStart(e)} value={this.state.start}/>
                <textarea onChange={e => this.changeMonthly(e)} value={this.state.monthly}/>
                <button onClick={this.refreshStockData.bind(this)}>
                    Load
                </button>
                <div style={{display: 'block'}}>
                    {top}
                    {gridRows}
                </div>
            </div>
        );
    }
}

export default App;
