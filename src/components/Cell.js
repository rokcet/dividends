import React from "react";

const cellStyle = {
    borderBottom: '1px solid black',
    borderRight: '1px solid black',
    width: '100px',
    textAlign: 'right',
    fontFamily: 'Courier, monospace',
    display: 'block'

};

function Cell(props){
    let style;
    if(props.header){
        style = {
            borderBottom: '1px solid black',
            borderRight: '1px solid black',
            width: '100px',
            textAlign: 'center',
            fontFamily: 'Courier, monospace'
        }
    }
    else
        style = cellStyle;

    return(
        <div style={style}>
            {props.children}
        </div>
    );
}

export default Cell;