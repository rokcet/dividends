import React from 'react';

import Cell from './Cell';

function round2(val){
    return Math.round(val*100)/100;
}

function GridRow({step}){
    const color = step.month % 2 === 0 ? '#666688': '#8888AA';
    const time = `${step.month}/${step.year}`;
    return (
        <div style={{backgroundColor: color, display: 'flex', overflow: 'hide', flex: 1}}>
            <Cell>
                {time}
            </Cell>
            <Cell>
                {round2(step.dividendPay)}
            </Cell>
            <Cell>
                {round2(step.shares)}
            </Cell>
            <Cell>
                {round2(step.leftover)}
            </Cell>
            <Cell>
                {round2(step.invested)}
            </Cell>
            <Cell>
                {round2(step.total)}
            </Cell>

        </div>
    );
}



export default GridRow;